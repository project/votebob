<?php

function _phptemplate_variables($hook, $vars = array()) {
  switch ($hook) {
    case 'page':
      $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
      $languages = array(
        l('English', $url),
        l('Espa&#241;ol', 'http://babelfish.altavista.com/babelfish/tr?doit=done&url='. $url .'/&lp=en_es', NULL, NULL, NULL, NULL, TRUE)
      );
      $vars['translate'] .= '<strong>' . t('Translate Page') . ':</strong> ';
      $vars['translate'] .= theme_item_list($languages);
      break;
  }
  return $vars;
}

function votebob_regions() {
  return array(
    'right' => t('right sidebar'),
    'footer' => t('footer')
  );
}