
VoteBob theme by Matt Koglin - http://www.antinomia.com/

To install, untar the 'votebob' directory into your sites/all/themes folder and enable the theme at admin/build/themes.

TIPS:

To change the exact placement of your logo and site title, edit the following positions in style.css:
  >  #banner .logo {
  >    position: relative;
  >    left: 20px;
  >    top: 30px;
  >  }

