<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>
<body>
  <div id="translate">
    <?php print $translate; ?>
  </div>
  <div id="wrapper">

    <div id="banner">
      <span id="logo-title">
        <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" style="vertical-align: middle;" alt="<?php print t('Home') ?>" /></a><?php } ?>
        <?php if ($site_name) { ?><h1 class="site-name"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      </span>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
    </div>

    <?php if (isset($primary_links)) { ?><div id="primary-links"><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?></div><?php } ?>

    <div id="main">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <?php print $breadcrumb ?>
      <h1 class="title"><?php print $title ?></h1>
      <div class="tabs"><?php print $tabs ?></div>
      <?php print $help ?>
      <?php print $messages ?>
      <?php print $content; ?>
      <?php print $feed_icons; ?>
    </div>
    
    <?php if ($sidebar_right) { ?><div id="sidebar-right"><?php print $sidebar_right ?></div><?php } ?>

    <br style="clear: both;" />

    <div id="footer" align="center"><?php print $footer_message ?></div>

  </div>
<?php print $closure ?>
</body>
</html>